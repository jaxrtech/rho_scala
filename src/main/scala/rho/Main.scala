package rho

import org.parboiled2._

import scala.io.StdIn
import scala.util.{Failure, Success}


object Main extends App {

  def readln() = {
    val line = StdIn.readLine()
    if (!line.isEmpty) {
      Some(line)
    } else {
      None
    }
  }

  var line = None : Option[String]
  do {
    print("> ")
    line = readln()
    line.foreach { line =>
      val parser = new RhoParser(line)

      val result = parser.Program.run()
      result match {
        case Success(expr) =>
          println(expr)

        case Failure(error: ParseError) =>
          val message = parser.formatError(error, new ErrorFormatter(showTraces = true))
          Console.err.println(message)

        case Failure(error) =>
          throw error
      }
    }
  } while (line.isDefined)

}

case class RhoLetExpression(name: String, value: Int)

case class RhoString(value: String)


class RhoParser(val input: ParserInput) extends Parser with StringBuilding {
  import RhoParser._
  import CharPredicate.HexDigit

  def Program = rule {
    LetExpression ~ EOI
  }

  def LetExpression = rule {
    "let" ~ WS ~ RIdentifier ~ ws ~ '=' ~ ws ~ RInt ~> RhoLetExpression
  }

  def RIdentifier = rule {
    capture(atomic(oneOrMore(CharPredicate.Alpha) ~ zeroOrMore(CharPredicate.AlphaNum)))
  }

  def RInt = rule {
    capture(Digits) ~> (_.toInt)
  }

  def Digits = rule {
    oneOrMore(CharPredicate.Digit)
  }

  def RString = rule {
    RStringUnwrapped ~> RhoString
  }

  def RStringUnwrapped = rule {
    '"' ~ clearSB() ~ Characters ~ '"' ~ ws ~ push(sb.toString)
  }

  def Characters = rule {
    zeroOrMore(NormalChar | '\\' ~ EscapedChar)
  }

  def NormalChar = rule {
    !QuoteBackslash ~ ANY ~ appendSB()
  }

  def EscapedChar = rule(
    QuoteSlashBackSlash ~ appendSB()
      | 'b' ~ appendSB('\b')
      | 'f' ~ appendSB('\f')
      | 'n' ~ appendSB('\n')
      | 'r' ~ appendSB('\r')
      | 't' ~ appendSB('\t')
      | Unicode ~> { code => sb.append(code.asInstanceOf[Char]); () }
  )

  def Unicode = rule {
    'u' ~ capture(HexDigit ~ HexDigit ~ HexDigit ~ HexDigit) ~> (java.lang.Integer.parseInt(_, 16))
  }

  def ws = rule {
    zeroOrMore(Whitespace) | fail("whitespace")
  }

  def WS = rule {
    oneOrMore(Whitespace) | fail("whitespace")
  }
}

object RhoParser {
  val Whitespace = CharPredicate(" \t\n")
  val QuoteBackslash = CharPredicate("\"\\")
  val QuoteSlashBackSlash = QuoteBackslash ++ "/"
}